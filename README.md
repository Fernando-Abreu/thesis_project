# thesis_project

## Requirements

*[python](https://www.python.org/) >= 3.6
*[postgres](https://www.postgresql.org/)
*[postgis](https://postgis.net/) - Is an postgres extention to work with geometries and geography
*[node](https://nodejs.org/en/) 


# Database

## Installation
After you have PostgreSQL installed, create a database called `thesis`. Then, install and follow PostGIS guide to enable it.
Once you have finished run each migration inside `database_scripts` in the numbered order.
Eg

```
psql -d thesis -f database_scripts/v01_initial.sql
```

# Backend

## Installation

If you don't want to polute your global space, make sure to use some virtual env. Then, run 

```
pip install requirements.txt
```


## Running

Go to `backend` folder and run 

```
python run.py
```

# Frontend

## Installation

With Node installed go to the frontend folder and install dependencies with the command

```
npm install
```

## Running

Make sure you have the backend running first, then run the following command:

```
npm start 
```

or, if you have `yarn` installed you can run 

```
yarn start 
```

##
# TODO

* Add different segment sizes
* Add itinarary table which will have a origin and destination
